## Mini app simulador de pagos

Hola, un gusto. Realmente no soy muy bueno con el Wocomerce, asi que intenté todo lo que pude. Para esta prueba utilice **Laravel 7** y **Vue.JS**, ya que ustedes pedian que no existiera tanta complejidad de la hora de iniciar la app. Bueno, me considero bastante bueno con Laravel, de hecho, fue mi primer framework.

Pero comencemos con los pasos a seguir.

1. Primero necesitaremos instalar las dependencias principales de Laravel. (recuerden estar en la raiz del proyecto)

```
    composer install (o update)
```

2. Después tendremos que realizar nuestro propio archivo **.env**. Es importante que rellenen con los datos de su base de datos (yo use mysqli) y claro, los datos del gestor del mailer (yo recomiendo utilizar **mailtrap**)

```
    cp .env.example .env
```

3. Ahora, necesitamos insertar nuestras tablas en nuestra respectiva base de datos

```
    php artisan migrate
```

4. Es necesario para poder utilizar las autentificaciones que se realice el siguiente comando, importante, los datos que te dará es necesario que los coloques en tu archivo **env** en sus respectivas variables (**PASSPORT_PERSONAL_ACCESS_CLIENT_ID** y **PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET**)

```
    php artisan passport:install
```

5. Ya con todo eso, faltaría instalar las dependencias de VueJS.

```
    npm install
```

6. Después, si recuerdo bien, utilizar el comando para iniciar la compilación de **webpack**.

```
    npm run dev / npm run watch
```

7. Y por ultimo, pero no menos importante sería realizar el comando para iniciar el servidor. (A menos claro que tengan ya un gestor como apache, yo usé laragon para hacer host virtuales y demás)

```
    php artisan serve
```

Y eso sería todo en un principio, ¡disfruten de la app, le dedique mucho esfuerzo!
