<?php

namespace App\Providers;

use App\Http\Interfaces\Auth\AuthInterface;
use Illuminate\Support\ServiceProvider;
use App\Http\Interfaces\Wallet\WalletInterface;
use App\Http\Interfaces\Payment\PaymentInterface;
use App\Http\Repositories\Auth\AuthRepository;
use App\Http\Repositories\Wallet\WalletRepository;
use App\Http\Repositories\Payment\PaymentRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(WalletInterface::class, WalletRepository::class);
        $this->app->bind(PaymentInterface::class, PaymentRepository::class);
        $this->app->bind(AuthInterface::class, AuthRepository::class);
    }
}
