<?php

namespace App\Http\Repositories\Auth;

use App\Http\Interfaces\Auth\AuthInterface;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Http\Requests\Auth\RegisterRequest;


class AuthRepository implements AuthInterface
{
    private $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function signup(RegisterRequest $request)
    {
        $user = $this->model->create([
            'name' => $request->name,
            'email' => $request->email,
            'cid' => $request->cid,
            'phone' => $request->phone,
            'password' => bcrypt($request->password)
        ]);

        $user->wallet()->create();

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $userTotal = $this->model->with('wallet')->find(auth()->user()->id);

        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'session' => $request->session()->get('_token'),
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'user' => $userTotal
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function profile(Request $request)
    {
        return response()->json($request->user());
    }
}
