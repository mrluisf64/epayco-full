<?php

namespace App\Http\Repositories\Payment;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionConfirmation;
use App\Http\Interfaces\Payment\PaymentInterface;
use App\Http\Requests\Payment\PaymentConfirmRequest;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class PaymentRepository implements PaymentInterface
{
    use AuthorizesRequests;

    private $model;

    public function __construct(Transaction $transaction)
    {
        $this->model = $transaction;
    }

    public function generateEmail(PaymentConfirmRequest $request)
    {
        $transaction = $request->user()->transactions()->create([
            'state' => 'pending',
            'token' => generateRandomString(6),
            'detail' => 'Confirmation Payment: ' . $request->product,
            'session_id' => $request->session,
            'mount' => $request->value,
        ]);

        Mail::to($request->user())->send(new TransactionConfirmation($transaction));
        return response()->json([
            'message' => 'Send confirm email successfully!'
        ], 200);
    }

    public function confirmPayment(Request $request)
    {
        $transaction = $this->model->with('user.wallet')->where('token', $request->token)->first();
        if (!$transaction) {
            return response()->json([
                'message' => 'No exists transaction'
            ], 400);
        }
        $wallet = $transaction->user->wallet;

        $this->authorize('view', [$transaction, $request]);

        if ($wallet->balance - $transaction->mount > 0) {
            $wallet->update([
                'balance' => $wallet->balance - $transaction->mount
            ]);

            $transaction->update([
                'state' => 'success',
                'token' => null,
            ]);

            return response()->json([
                'message' => 'Buy successfully!'
            ], 200);
        }
        return response()->json([
            'message' => 'Not money'
        ], 400);
    }
}
