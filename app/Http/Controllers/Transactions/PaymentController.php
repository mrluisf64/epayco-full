<?php

namespace App\Http\Controllers\Transactions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\PaymentConfirmRequest;
use App\Http\Repositories\Payment\PaymentRepository;

class PaymentController extends Controller
{
    private $repository;

    public function __construct(PaymentRepository $payment)
    {
        $this->repository = $payment;
    }


    public function show(PaymentConfirmRequest $request)
    {
        return $this->repository->generateEmail($request);
    }

    public function store(Request $request)
    {
        return $this->repository->confirmPayment($request);
    }
}
