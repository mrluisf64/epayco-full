<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Repositories\Auth\AuthRepository;

class AuthController extends Controller
{
    private $repository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->repository = $authRepository;
    }

    /**
     * Registro de usuario
     */
    public function signUp(RegisterRequest $request)
    {
        return $this->repository->signup($request);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(LoginRequest $request)
    {
        return $this->repository->login($request);
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        return $this->repository->logout($request);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return $this->repository->profile($request);
    }
}
