<?php

namespace App\Http\Interfaces\Auth;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;

interface AuthInterface
{
    public function signup(RegisterRequest $request);
    public function login(LoginRequest $request);
    public function logout(Request $request);
    public function profile(Request $request);
}
