<?php

namespace App\Http\Interfaces\Wallet;

use Illuminate\Http\Request;

interface WalletInterface {
    public function getWallet();
    public function getWalletById($id);
    public function rechargeWallet(Request $request, $id);
    public function consultBalanceWallet(Request $request);
}
