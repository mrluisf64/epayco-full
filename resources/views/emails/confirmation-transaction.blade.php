<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirmation Transaction</title>
</head>
<body>
        <p>You transaction detail: {{ $transaction->detail }}</p>
        <p>Cost: {{ $transaction->mount }}</p>

        <p>Your code for transaction: {{$transaction->token}}</p>
</body>
</html>
