import axios from "axios";

class PaymentService {
    sendConfirm(data) {
        return axios.post("api/payment/confirm", data);
    }

    buyObject(data) {
        return axios.post("/api/payment/confirmation-payment", data);
    }
}

export default new PaymentService();
