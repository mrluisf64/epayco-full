import axios from 'axios'

class WalletService {
    recharge(data, id) {
        return axios.put('api/wallet/'+ id, data);
    }

    consult(data) {
        return axios.post('api/wallet', data);
    }

}

export default new WalletService();
