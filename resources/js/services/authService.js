import axios from "axios";
class AuthService {
    login(data) {
        return axios.post("api/auth/login", data);
    }

    register(data) {
        return axios.post("api/auth/signup", data);
    }

    profile() {
        return axios.get("api/auth/profile");
    }
}

export default new AuthService();
