import React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Auth from "./Auth/Index";
import Welcome from "./Welcome";
import { NotificationContainer } from "react-notifications";
import Store from "./Store/Index";
import Sidebar from "./Layouts/Sidebar";

const App = () => {
    return (
        <Router>
            <NotificationContainer />
            <Switch>
                <Route path="/" exact>
                    <Welcome />
                </Route>
                <Route path="/welcome">
                    <Auth />
                </Route>
                <Route path="/store">
                    <Sidebar />
                    <Store />
                </Route>
            </Switch>
        </Router>
    );
};

export default App;
