import React from "react";
import { Modal, Form, Input, Button } from "antd";
import { sendNotification, sendNotifyError } from "../../helpers/notify";
import PaymentService from "../../services/paymentService";
import { getDataDecrypt } from "../../helpers/encrypt";

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16
    }
};

const PaymentConfirmModal = props => {
    const onFinish = values => {
        const form = {
            token: values.token,
            session: getDataDecrypt("session")
        };

        PaymentService.buyObject(form)
            .then(() => {
                sendNotification(
                    "success",
                    "Su compra ha sido exitosa!",
                    "¡Solicitud Exitosa!"
                );
                props.toggleModal();
            })
            .catch(e => {
                sendNotification(
                    "error",
                    e.response.data.message,
                    "¡Hubo un problema!"
                );
                if (e.response.data.message == "Not money") {
                    props.toggleModal();
                }
            });
    };

    const onFinishFailed = errorInfo => {
        console.log("Failed:", errorInfo);
    };

    return (
        <Modal
            title="Confirmar su compra"
            visible={props.modal}
            footer={null}
            cancelButtonProps={null}
        >
            <Form
                {...layout}
                name="basic"
                initialValues={{
                    remember: true
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Clave única"
                    name="token"
                    rules={[
                        {
                            required: true,
                            message:
                                "Para confirmar ingrese la clave que le llegó al correo"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Confirmar Compra
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default PaymentConfirmModal;
