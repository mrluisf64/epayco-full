import React from "react";
import { Card, Col, Row } from "antd";
import { Link } from "react-router-dom";

const Welcome = () => {
    return (
        <>
            <Row gutter={16} justify="center">
                <Col span={20}>
                    <Card title="Bienvenido">
                        Bienvenido a la aplicación, para comenzar primero inicie
                        sesión y registrese. Haciendo{" "}
                        <Link to="/welcome">click aquí</Link>
                    </Card>
                </Col>
            </Row>
        </>
    );
};

export default Welcome;
