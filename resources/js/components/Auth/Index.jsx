import React from "react";
import { Row, Col } from "antd";
import Login from "./Login";
import Register from "./Register";

const Auth = () => {
    return (
        <>
            <Row justify="center">
                <Col span={11}>
                    <Login />
                </Col>
                <Col span={11}>
                    <Register />
                </Col>
            </Row>
        </>
    );
};

export default Auth;
