import React from "react";
import { Card, Form, Input, Button } from "antd";
import { setDataDecrypt } from "../../helpers/encrypt";
import AuthService from "../../services/authService";
import { useHistory } from "react-router-dom";
import { sendNotification, sendNotifyError } from "../../helpers/notify";

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 }
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 }
};

const Login = () => {
    let history = useHistory();

    const onFinish = values => {
        AuthService.login(values).then(response => {
            const { data } = response;
            const { user, access_token } = data;

            setDataDecrypt("session", access_token);
            setDataDecrypt("profile", user);

            sendNotification(
                "success",
                "Usted ha iniciado sesión correctamente",
                "¡Solicitud Exitosa!"
            );

            history.push("/store");
        });
    };

    const onFinishFailed = errorInfo => {
        sendNotifyError();
    };

    return (
        <Card title="Incie Sesión" style={{ marginTop: 25, marginRight: 5 }}>
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Correo electrónico"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingrese su correo electrónico"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Contraseña"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingrese su contraseña"
                        }
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Iniciar
                    </Button>
                </Form.Item>
            </Form>
        </Card>
    );
};

export default Login;
