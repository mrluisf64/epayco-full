import React from "react";
import { Card, Form, Input, Button } from "antd";
import AuthService from "../../services/authService";
import { setDataDecrypt } from "../../helpers/encrypt";
import { sendNotification, sendNotifyError } from "../../helpers/notify";
import { useHistory } from "react-router-dom";

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 }
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 }
};

const Register = () => {
    let history = useHistory();

    const onFinish = values => {
        AuthService.register(values).then(response => {
            sendNotification(
                "success",
                "Usted se ha registrado correctamente, ¡ahora inicie sesión!",
                "¡Solicitud Exitosa!"
            );
        });
    };

    const onFinishFailed = errorInfo => {
        sendNotifyError();
    };

    return (
        <Card title="Registrese" style={{ marginTop: 25, marginLeft: 5 }}>
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Nombre"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingrese su nombre"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Número de documento"
                    name="cid"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingrese su ID"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Correo electrónico"
                    name="email"
                    rules={[
                        {
                            required: true,
                            type: "email",
                            message:
                                "Por favor ingrese su email y que tenga el formato correcto"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Telefóno"
                    name="phone"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingrese su numero telefonico"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Contraseña"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingrese su contraseña"
                        }
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Enviar
                    </Button>
                </Form.Item>
            </Form>
        </Card>
    );
};

export default Register;
