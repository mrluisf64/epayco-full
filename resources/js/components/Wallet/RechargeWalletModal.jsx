import React from "react";
import { Modal, Form, Input, Button, Checkbox } from "antd";
import WalletService from "../../services/walletService";
import { getDataDecrypt } from "../../helpers/encrypt";
import { sendNotification } from "../../helpers/notify";

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16
    }
};

const RechargeWalletModal = props => {
    const onFinish = values => {
        const profile = getDataDecrypt("profile");
        WalletService.recharge(values, profile.wallet.id).then(() => {
            sendNotification(
                "success",
                "Ahora podrá gastar su dinero",
                "¡Solicitud Exitosa!"
            );
            props.toggleRecharge();
        });
    };

    const onFinishFailed = errorInfo => {
        console.log("Failed:", errorInfo);
    };

    return (
        <Modal
            title="Recargar Saldo"
            visible={props.rechargeModal}
            onOk={props.toggleRecharge}
            onCancel={props.toggleRecharge}
            footer={null}
        >
            <Form
                {...layout}
                name="basic"
                initialValues={{
                    remember: true
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Documento de Identidad"
                    name="cid"
                    rules={[
                        {
                            required: true,
                            message: "Ingrese su ID!"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Numero Telefonico"
                    name="phone"
                    rules={[
                        {
                            required: true,
                            message: "Ingrese su numero de telefono"
                        }
                    ]}
                >
                    <Input type="number" />
                </Form.Item>

                <Form.Item
                    label="Saldo a rellenar"
                    name="value"
                    rules={[
                        {
                            required: true,
                            message: "Ingrese un valor adecuado"
                        }
                    ]}
                >
                    <Input type="number" />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Recargar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default RechargeWalletModal;
