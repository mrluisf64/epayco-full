import React from "react";
import { Modal, Form, Input, Button, Checkbox } from "antd";
import WalletService from "../../services/walletService";
import { sendNotification } from "../../helpers/notify";

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16
    }
};

const ConsultWalletModal = props => {
    const onFinish = values => {
        WalletService.consult(values).then(response => {
            const { data } = response;
            const { json } = data;

            sendNotification(
                "success",
                "Su saldo disponible es de: " + json.balance,
                "¡Solicitud Exitosa!"
            );
            props.toggleConsult();
        });
    };

    const onFinishFailed = errorInfo => {
        console.log("Failed:", errorInfo);
    };

    return (
        <Modal
            title="Consultar Saldo"
            visible={props.consultModal}
            onOk={props.toggleConsult}
            onCancel={props.toggleConsult}
            footer={null}
        >
            <Form
                {...layout}
                name="basic"
                initialValues={{
                    remember: true
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Documento de Identidad"
                    name="cid"
                    rules={[
                        {
                            required: true,
                            message: "Ingrese su ID!"
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Numero Telefonico"
                    name="phone"
                    rules={[
                        {
                            required: true,
                            message: "Ingrese su numero de telefono"
                        }
                    ]}
                >
                    <Input type="number" />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Recargar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ConsultWalletModal;
