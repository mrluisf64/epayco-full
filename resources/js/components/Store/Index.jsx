import React, { useEffect, useState } from "react";
import { Card, Table, Button } from "antd";
import PaymentService from "../../services/paymentService";
import { sendNotification } from "../../helpers/notify";
import PaymentConfirmModal from "../Payment/PaymentConfirmModal";
import { getDataDecrypt } from "../../helpers/encrypt";
import { useHistory } from "react-router";

const Store = () => {
    const [loading, setLoading] = useState(true);
    const [items, setItems] = useState();
    const [modal, setModal] = useState(false);

    let history = useHistory();

    const toggleModal = () => {
        setModal(!modal);
    };

    useEffect(() => {
        const profile = getDataDecrypt("profile");
        if (!profile) {
            history.push("/");
            sendNotification(
                "warn",
                "¡Debe de iniciar sesión antes de entrar a la tienda!",
                "¡Aviso Importante!"
            );
        }
    }, []);

    const columns = [
        { title: "Nombre", dataIndex: "name", key: "name" },
        { title: "Descripción", dataIndex: "description", key: "description" },
        { title: "Precio", dataIndex: "price", key: "price" },
        { title: "Categoría", dataIndex: "category", key: "category" },
        {
            title: "Opciones",
            key: "options",
            render: (text, record) => (
                <Button onClick={() => onSendEmail(record)} type="primary">
                    Comprar
                </Button>
            )
        }
    ];

    const onSendEmail = data => {
        const body = {
            product: data.name,
            value: data.price,
            session: getDataDecrypt("session")
        };

        PaymentService.sendConfirm(body)
            .then(() => {
                sendNotification(
                    "info",
                    "Necesita revisar su correo para ingresar el código que le hemos enviado",
                    "¡Email de confirmación en espera!"
                );
                toggleModal();
            })
            .catch(e => {
                sendNotification("error", e.response.data, "Hubo un problema");
            });
    };

    useEffect(() => {
        setItems([
            {
                name: "Balón",
                description: "Balon bonito",
                price: 50,
                category: "Soccer"
            },
            {
                name: "Mesa",
                description: "Mesa de madera",
                price: 150,
                category: "Hogar"
            },
            {
                name: "Silla",
                description: "Silla de madera",
                price: 73,
                category: "Hogar"
            },
            {
                name: "Crypto",
                description: "Bitcoin",
                price: 50000,
                category: "Internet"
            }
        ]);
        setLoading(false);
    }, []);

    return (
        <>
            <Card
                title="Tienda de objetos"
                style={{ marginTop: 17 }}
                loading={loading}
            >
                <Table
                    columns={columns}
                    dataSource={items}
                    pagination={{
                        defaultPageSize: 10,
                        showSizeChanger: true,
                        pageSizeOptions: ["10", "20", "30"]
                    }}
                />
            </Card>
            <PaymentConfirmModal toggleModal={toggleModal} modal={modal} />
        </>
    );
};

export default Store;
