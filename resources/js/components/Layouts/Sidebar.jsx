import React, { useState } from "react";
import { Menu } from "antd";
import {
    MailOutlined,
    AppstoreOutlined,
    SettingOutlined
} from "@ant-design/icons";
import RechargeWalletModal from "../Wallet/RechargeWalletModal";
import ConsultWalletModal from "../Wallet/ConsultWalletModal";
import { useHistory } from "react-router";
import { sendNotification } from "../../helpers/notify";

const Sidebar = () => {
    const [rechargeModal, setRechargeModal] = useState(false);
    const [consultModal, setConsultModal] = useState(false);

    let history = useHistory();

    const toggleRecharge = () => {
        setRechargeModal(!rechargeModal);
    };

    const toggleConsult = () => {
        setConsultModal(!consultModal);
    };

    const onLogout = () => {
        localStorage.clear();
        sendNotification(
            "success",
            "¡Usted ha cerrado sesión correctamente!",
            "Cierre de Sesión realizado"
        );
        history.push("/");
    };

    return (
        <>
            <Menu mode="horizontal">
                <Menu.Item
                    key="mail"
                    icon={<MailOutlined />}
                    onClick={toggleRecharge}
                >
                    Recargar Saldo
                </Menu.Item>
                <Menu.Item
                    key="app"
                    icon={<AppstoreOutlined />}
                    onClick={toggleConsult}
                >
                    Consultar Saldo
                </Menu.Item>
                <Menu.Item
                    key="logout"
                    icon={<SettingOutlined />}
                    onClick={onLogout}
                >
                    Cerrar Sesión
                </Menu.Item>
            </Menu>
            <RechargeWalletModal
                rechargeModal={rechargeModal}
                toggleRecharge={toggleRecharge}
            />
            <ConsultWalletModal
                toggleConsult={toggleConsult}
                consultModal={consultModal}
            />
        </>
    );
};

export default Sidebar;
