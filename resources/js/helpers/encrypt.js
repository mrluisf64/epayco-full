import CryptoJS from "crypto-js";

export function setDataDecrypt(key, value) {
    let secret = CryptoJS.AES.encrypt(JSON.stringify(value), "lts").toString();
    localStorage.setItem(key, secret);
}

export function getDataDecrypt(key) {
    let item = localStorage.getItem(key);
    if (item) {
        let bytes = CryptoJS.AES.decrypt(item, "lts");
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    }
}
