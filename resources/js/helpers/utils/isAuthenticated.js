import { getDataDecrypt } from "../encrypt";

const isAuthenticated = () => {
    return getDataDecrypt("profile") ? true : false;
};

export default isAuthenticated;
