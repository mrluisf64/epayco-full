import { NotificationManager } from "react-notifications";

export const sendNotification = (type, value, title) => {
    switch (type) {
        case "info":
            NotificationManager.info(value, title);
            break;
        case "success":
            NotificationManager.success(value, title);
            break;
        case "warning":
            NotificationManager.warning(value, title);
            break;
        case "error":
            NotificationManager.error(value, title);
            break;
        default:
            NotificationManager.warning("Ups!", "¡Algo ha salido mal!");
    }
};

export const sendNotifyError = () => {
    sendNotification(
        "error",
        "Ha ocurrido un problema, por favor verifique.",
        "¡Solicitud Erronea!"
    );
};
