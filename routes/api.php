<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('signup', 'Auth\AuthController@signUp');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'Auth\AuthController@logout');
        Route::get('user', 'Auth\AuthController@user');
    });
});

Route::group([
    'prefix' => 'wallet',
    'middleware' => ['auth:api']
], function () {
    Route::get('', 'Transactions\WalletController@index');
    Route::post('', 'Transactions\WalletController@store');
    Route::put('{id}', 'Transactions\WalletController@update');
});

Route::group([
    'prefix' => 'payment',
    'middleware' => ['auth:api']
], function () {
    Route::post('confirm', 'Transactions\PaymentController@show');
    Route::post('confirmation-payment', 'Transactions\PaymentController@store')->name('payment.confirmation');
});
